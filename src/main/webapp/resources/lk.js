var lk = {
  shopList: [

  ],
  shopItem: undefined,
  playerInfo: {
      id: 0,
      name: "(loading)",
      realmoney: 0
  }
};

function handleError(data) {
    if (data.error) {
        var error = data.error;
        switch (error.code) {
            case 4:
                doxfLogin();
                break;
            default:
                if (error.message)
                    alert("Error! " + error.code + ". " + error.message);
                else
                    alert("Error! " + error.code);
        }
        return true;
    }
    return false;
}

function doLogin(uname, pass) {
    $.ajax({
        dataType: "json",
        method: "post",
        url: "login",
        data:  {
            login: uname,
            password: pass
        },
        success: function(data) {
            if (!handleError(data)) {
                init();
            }
        }
    });
}

function login() {
    doLogin($("#login").val(), $("#password").val());
}

function logout() {
    $.ajax({
        dataType: "json",
        method: "post",
        url: "logout",
        success: function(data) {
            if (!handleError(data)) {
                init();
            }
        }
    });
}

function shopItemById(id) {
    for (var i = 0; i < lk.shopList.length; i++)
        if (lk.shopList[i].id == id)
            return lk.shopList[i];
    return undefined;
}

function shop_buy(amount) {
    $.ajax({
        dataType: "json",
        method: "post",
        url: "shop-buy",
        data: {
            itemId: lk.shopItem.id,
            amount: amount
        },
        success: function(data) {
            if (!handleError(data)) {
                alert("С покупкой!");
            }

            loadPlayerInfo();
        }
    });
}

function shop_buydialog(id) {
    lk.shopItem = shopItemById(id);
    alert("Покупаем " + lk.shopItem.name);

    var amount = prompt("Введите количество");

    shop_buy(amount);
}

function renderShopRow(rowdata) {
    return '<tr>' +
        '<td><img src="' + rowdata.image + '"/></td>' +
        '<td>' + rowdata.name + '</td>' +
        '<td>' + rowdata.price + '</td>' +
        '<td>' + rowdata.category + '</td>' +
        '<td><a onclick="shop_buydialog(' + rowdata.id + ');">Купить</a></td>' +
        '</tr>';
}

function renderShopTable(rowsdata) {
    var rows = [];
    for (var i = 0; i < rowsdata.length; i++) {
        rows.push(renderShopRow(rowsdata[i]));
    }
    return '<table><th>Картинка</th><th>Название</th><th>Цена</th><th>Категория</th><th>Действия</th>' +
        rows.join("\n") + '</table>';
}

function loadShop() {
    $.getJSON("shop-list", function(data) {
        if (!handleError(data)) {
            lk.shopList = data.list;
            $("#shop-list").html(renderShopTable(lk.shopList));
        }
    });
}

function loadPlayerInfo() {
    $.getJSON("player-info", function(data) {
        if (!handleError(data)) {
            lk.playerInfo = data;
            $("#pinfo-money").text(data.realmoney);
            $("#pinfo-name").text(data.name);
            $("#content").show();
        }
    });
}

function doxfLogin() {
    $.get("http://pixelsky.ru/xf-savekey.php", function(data) {
        data = $.parseJSON(data);
        if (data.login) {
            var username = data.username;
            var authkey = data.authkey;
            doLogin(username, authkey);
        } else {
            alert("Вы не залогинены");
        }
    });

}

function showContent() {
    $("#auth").hide();
    $("#content").show();
}

function init() {
    showContent();

    loadPlayerInfo();
    loadShop();
}

$(function() {
    $('#skin-cloak-form').ajaxForm(function(data) {
        if (!handleError(data)) {
            alert("OK!");
        }
    });

    init();
});