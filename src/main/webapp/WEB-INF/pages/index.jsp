<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>PixelSky lk</title>
    <script src="resources/jquery.min.js"></script>
    <script src="resources/jquery.form.min.js"></script>
    <script src="resources/lk.js"></script>
</head>
<body>
    <div id="auth">
        <input type="text" id="login"/><br/>
        <input type="password" id="password"/><br/>
        <input type="button" value="Вход" onclick="login();"/>
    </div>
    <div id="content">
        <a onclick="logout();">Выйти</a><br/><br/>

        Привет, <span id="pinfo-name">(загрузка)</span><br/>
        Твой баланс: <span id="pinfo-money">(загрузка)</span><br/>

        <form id="skin-cloak-form" method="post" enctype="multipart/form-data" action="upload">
            Скин: <input type="file" name="skin"><br/>
            Плащ: <input type="file" name="cloak"><br/>
            <input type="submit" value="Обновить"/>
        </form>

        <div id="shop">
            Немного <s>бесполезных</s> вещей в магазине:
            <div id="shop-list">

            </div>
        </div>
    </div>
</body>
</html>