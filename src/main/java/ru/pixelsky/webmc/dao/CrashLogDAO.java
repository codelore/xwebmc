package ru.pixelsky.webmc.dao;

import ru.pixelsky.webmc.data.CrashLog;
import java.util.List;

public interface CrashLogDAO {
    void persist(CrashLog crashLog);
    List<CrashLog> find(String username);
    List<CrashLog> find();
}
