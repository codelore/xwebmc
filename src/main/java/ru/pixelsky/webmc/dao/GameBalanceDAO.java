package ru.pixelsky.webmc.dao;

import ru.pixelsky.webmc.data.GameBalance;

import java.math.BigDecimal;
import java.util.List;

public interface GameBalanceDAO {
    void updateBalance(String username, BigDecimal balance);
    GameBalance findBalance(String username);
    List<GameBalance> getBalanceTop(int limit);
}
