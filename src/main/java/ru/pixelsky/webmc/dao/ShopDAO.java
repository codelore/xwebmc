package ru.pixelsky.webmc.dao;

import ru.pixelsky.webmc.data.ShopCartItem;
import ru.pixelsky.webmc.data.ShopItem;

import java.util.List;

public interface ShopDAO {
    void persistShopItem(ShopItem item);
    ShopItem findShopItem(int itemId);
    List<ShopItem> listShopItems();

    void persistCartItem(ShopCartItem item);
}
