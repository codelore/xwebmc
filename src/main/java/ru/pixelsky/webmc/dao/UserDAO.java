package ru.pixelsky.webmc.dao;

import ru.pixelsky.webmc.data.UserData;

public interface UserDAO {
    void persist(UserData userData);
    UserData find(String name);
    UserData find(Long id);
}
