package ru.pixelsky.webmc.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.dao.ShopDAO;
import ru.pixelsky.webmc.dao.UserDAO;
import ru.pixelsky.webmc.data.ShopCartItem;
import ru.pixelsky.webmc.data.ShopItem;
import ru.pixelsky.webmc.data.UserData;
import ru.pixelsky.webmc.service.DatabaseService;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@SuppressWarnings("JpaQueryApiInspection")
@Service
public class ShopDAOImpl implements ShopDAO {
    @Autowired
    private DatabaseService db;

    @Override
    public void persistShopItem(ShopItem item) {
        EntityManager mng = db.getEntityManager();
        try {
            mng.getTransaction().begin();
            mng.persist(item);
            mng.getTransaction().commit();
        } finally {
            mng.close();
        }
    }

    @Override
    public ShopItem findShopItem(int itemId) {
        EntityManager mng = db.getEntityManager();
        try {
            TypedQuery<ShopItem> query = mng.createQuery("SELECT data FROM ru.pixelsky.webmc.data.ShopItem data WHERE id=:id", ShopItem.class);
            query.setParameter("id", (long)itemId);
            List<ShopItem> list = query.getResultList();
            return list.isEmpty() ? null : list.get(0);
        } finally {
            mng.close();
        }
    }

    @Override
    public List<ShopItem> listShopItems() {
        EntityManager mng = db.getEntityManager();
        try {
            TypedQuery<ShopItem> query = mng.createQuery("SELECT data FROM ru.pixelsky.webmc.data.ShopItem data", ShopItem.class);
            return query.getResultList();
        } finally {
            mng.close();
        }
    }

    @Override
    public void persistCartItem(ShopCartItem item) {
        EntityManager mng = db.getEntityManager();
        try {
            mng.getTransaction().begin();
            mng.persist(item);
            mng.getTransaction().commit();
        } finally {
            mng.close();
        }
    }
}
