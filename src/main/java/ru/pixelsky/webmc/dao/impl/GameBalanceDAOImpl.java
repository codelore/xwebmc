package ru.pixelsky.webmc.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.dao.GameBalanceDAO;
import ru.pixelsky.webmc.data.GameBalance;
import ru.pixelsky.webmc.service.DatabaseService;
import ru.pixelsky.webmc.utils.db.OrderType;
import ru.pixelsky.webmc.utils.db.QueryHelper;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

@SuppressWarnings("JpaQueryApiInspection")
@Service
public class GameBalanceDAOImpl implements GameBalanceDAO {
    @Autowired
    private DatabaseService db;

    private static final QueryHelper<GameBalance> QUERY_FIND = new QueryHelper<GameBalance>(GameBalance.class).addWhereParam("username");
    private static final QueryHelper<GameBalance> QUERY_TOP = new QueryHelper<GameBalance>(GameBalance.class).setOrder("balance", OrderType.DESC);

    @Override
    public void updateBalance(String username, BigDecimal balance) {
        EntityManager mng = db.getEntityManager();
        try {
            mng.getTransaction().begin();
            GameBalance gameBalance = QUERY_FIND.getResult(mng, "username", username);
            if (gameBalance == null) {
                gameBalance = new GameBalance();
                gameBalance.setUsername(username);
            }

            gameBalance.setBalance(balance);
            mng.persist(gameBalance);
            mng.getTransaction().commit();
        } finally {
            mng.close();
        }
    }

    @Override
    public GameBalance findBalance(String username) {
        return QUERY_FIND.getResult(db, "username", username);
    }

    @Override
    public List<GameBalance> getBalanceTop(int limit) {
        return QUERY_TOP.getResults(db, limit);
    }
}
