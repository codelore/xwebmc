package ru.pixelsky.webmc.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.dao.UserDAO;
import ru.pixelsky.webmc.data.UserData;
import ru.pixelsky.webmc.service.DatabaseService;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@SuppressWarnings("JpaQueryApiInspection")
@Service
public class UserDAOImpl implements UserDAO {
    @Autowired
    private DatabaseService db;

    @Override
    public void persist(UserData userData) {
        EntityManager mng = db.getEntityManager();
        try {
            mng.getTransaction().begin();
            mng.persist(userData);
            mng.getTransaction().commit();
        } finally {
            mng.close();
        }
    }

    @Override
    public UserData find(String name) {
        EntityManager mng = db.getEntityManager();
        try {
            TypedQuery<UserData> query = mng.createQuery("SELECT data FROM ru.pixelsky.webmc.data.UserData data WHERE name=:name", UserData.class);
            query.setParameter("name", name);
            List<UserData> list = query.getResultList();
            return list.isEmpty() ? null : list.get(0);
        } finally {
            mng.close();
        }
    }

    @Override
    public UserData find(Long id) {
        EntityManager mng = db.getEntityManager();
        try {
            TypedQuery<UserData> query = mng.createQuery("SELECT data FROM ru.pixelsky.webmc.data.UserData data WHERE id=:id", UserData.class);
            query.setParameter("id", id);
            List<UserData> list = query.getResultList();
            return list.isEmpty() ? null : list.get(0);
        } finally {
            mng.close();
        }
    }
}
