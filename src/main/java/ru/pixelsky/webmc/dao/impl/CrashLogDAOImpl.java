package ru.pixelsky.webmc.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.dao.CrashLogDAO;
import ru.pixelsky.webmc.data.CrashLog;
import ru.pixelsky.webmc.data.UserData;
import ru.pixelsky.webmc.service.DatabaseService;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@SuppressWarnings("JpaQueryApiInspection")
@Service
public class CrashLogDAOImpl implements CrashLogDAO {
    @Autowired
    private DatabaseService db;

    @Override
    public void persist(CrashLog crashLog) {
        EntityManager mng = db.getEntityManager();
        try {
            mng.getTransaction().begin();
            mng.persist(crashLog);
            mng.getTransaction().commit();
        } finally {
            mng.close();
        }
    }

    @Override
    public List<CrashLog> find(String name) {
        EntityManager mng = db.getEntityManager();
        try {
            TypedQuery<CrashLog> query = mng.createQuery("SELECT data FROM ru.pixelsky.webmc.data.CrashLog data WHERE user=:name", CrashLog.class);
            query.setParameter("name", name);
            return query.getResultList();
        } finally {
            mng.close();
        }
    }

    @Override
    public List<CrashLog> find() {
        EntityManager mng = db.getEntityManager();
        try {
            TypedQuery<CrashLog> query = mng.createQuery("SELECT data FROM ru.pixelsky.webmc.data.CrashLog data", CrashLog.class);
            return query.getResultList();
        } finally {
            mng.close();
        }
    }
}
