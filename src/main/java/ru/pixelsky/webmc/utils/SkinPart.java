package ru.pixelsky.webmc.utils;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Taken from RcBars
 *
 * Skin size: 64x32
 * Tile size = 4 pixels
 *
 * For other skin sizes tile size is proportional to skin size
 */
public class SkinPart {
    public static SkinPart BODY_FRONT = new SkinPart(5, 5, 2, 3);
    public static SkinPart HEAD_FRONT = new SkinPart(2, 2, 2, 2);
    public static SkinPart HEAD_LAYER_FRONT = new SkinPart(10, 2, 2, 2);
    public static SkinPart ARM_FRONT = new SkinPart(11, 5, 1, 3);
    public static SkinPart LEG_FRONT = new SkinPart(1, 5, 1, 3);

    /**
     * All coordinates are in tiles
     */
    private int tileX;
    private int tileY;
    private int width;
    private int height;

    public SkinPart(int tileX, int tileY, int width, int height) {
        this.tileX = tileX;
        this.tileY = tileY;
        this.width = width;
        this.height = height;
    }

    public int getTileX() {
        return tileX;
    }

    public int getTileY() {
        return tileY;
    }

    public BufferedImage getPartImage(BufferedImage skin) {
        int tileSizeX = skin.getWidth() / 16;
        int tileSizeY = skin.getHeight() / 8;
        int x = tileSizeX * tileX;
        int y = tileSizeY * tileY;
        int w = width * tileSizeX;
        int h = height * tileSizeY;

        BufferedImage newImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB_PRE);
        Graphics2D g2 = newImage.createGraphics();

        g2.drawImage(skin, -x, -y, null);
        g2.dispose();

        return newImage;
    }
}
