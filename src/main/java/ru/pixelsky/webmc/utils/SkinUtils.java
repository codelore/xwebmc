package ru.pixelsky.webmc.utils;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Taken from RcBars
 */
public class SkinUtils {
    /**
     * Get image of skin front
     *   HEAD
     *   HEAD
     * A BODY A
     * R BODY R
     * M BODY M
     *   BODY
     *   |L|L
     *   E|E|
     *   |G|G
     */
    public static BufferedImage getFrontSkin(BufferedImage fullSkin) {
        int tileSize = fullSkin.getWidth() / 16;
        BufferedImage head = SkinPart.HEAD_FRONT.getPartImage(fullSkin);
        BufferedImage body = SkinPart.BODY_FRONT.getPartImage(fullSkin);
        BufferedImage arm = SkinPart.ARM_FRONT.getPartImage(fullSkin);
        BufferedImage leg = SkinPart.LEG_FRONT.getPartImage(fullSkin);

        BufferedImage img = new BufferedImage(tileSize * 4, tileSize * 8, BufferedImage.TYPE_INT_ARGB_PRE);
        Graphics2D g2 = img.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);

        g2.drawImage(head, tileSize, 0, null);
        g2.drawImage(body, tileSize, 2 * tileSize, null);
        g2.drawImage( arm, 0, 2 * tileSize, null);
        g2.drawImage( arm, tileSize * 3, 2 * tileSize, null);
        g2.drawImage( leg, tileSize, 5 * tileSize, null);
        g2.drawImage( leg, tileSize * 2, 5 * tileSize, null);

        return img;
    }
}
