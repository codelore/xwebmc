package ru.pixelsky.webmc.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CryptUtils {

    public static String sha256(String text) {
        try {
            MessageDigest sha256 = sha256Digest();
            sha256.update(text.getBytes("UTF-8"));
            byte[] digest = sha256.digest();
            return bytesToHexString(digest, 32);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("No UTF-8 support", e);
        }
    }

    private static String bytesToHexString(byte[] bytes, int size) {
        BigInteger bigInt = new BigInteger(1, bytes);
        return addLeadingZeros(bigInt.toString(16), size);
    }

    private static String addLeadingZeros(String str, int len) {
        if (str.length() >= len)
            return str;
        int slen = str.length();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < len - slen; i++)
            builder.append('0');
        builder.append(str);
        return builder.toString();
    }

    private static MessageDigest sha256Digest() {
        try {
            return MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("No SHA-256", e);
        }
    }
}
