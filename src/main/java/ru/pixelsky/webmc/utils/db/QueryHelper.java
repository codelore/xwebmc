package ru.pixelsky.webmc.utils.db;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.LinkedList;
import java.util.List;

public class QueryHelper<T> {
    private Class<T> clazz;

    private List<String> whereParams = new LinkedList<String>();

    private String orderColumn;
    private OrderType orderType;

    private String jpaQuery;

    public QueryHelper(Class<T> clazz) {
        this.clazz = clazz;
        updateJpaQuery();
    }

    public QueryHelper<T> addWhereParam(String paramName) {
        whereParams.add(paramName + "=" + ":" + paramName);

        updateJpaQuery();
        return this;
    }

    public QueryHelper<T> setOrder(String orderColumn, OrderType type) {
        if (type == null && orderColumn != null)
            throw new NullPointerException("type == null!");

        this.orderColumn = orderColumn;
        this.orderType = type;

        updateJpaQuery();
        return this;
    }

    public String getJpaQuery() {
        return jpaQuery;
    }

    public T getResult(EntityManager em, String... params) {
        List<T> results = getResults(em, 1, params);
        if (!results.isEmpty()) {
            return results.get(0);
        } else return null;
    }

    public T getResult(EntityManagerSource ems, String... params) {
        EntityManager em = ems.getEntityManager();
        try {
            return getResult(em, params);
        } finally {
            em.close();
        }
    }

    public List<T>  getResults(EntityManagerSource ems, int limit, String... params) {
        EntityManager em = ems.getEntityManager();
        try {
            return getResults(em, limit, params);
        } finally {
            em.close();
        }
    }

    public List<T> getResults(EntityManager em, int limit, String... params) {
        if (params.length % 2 != 0)
            throw new IllegalArgumentException("params array length must be even");

        TypedQuery<T> query = em.createQuery(jpaQuery, clazz);
        if (limit > 0)
            query.setMaxResults(limit);

        for (int i = 0; i < params.length; i += 2) {
            query.setParameter(params[i], params[i + 1]);
        }

        return query.getResultList();
    }

    private void updateJpaQuery() {
        jpaQuery = "SELECT data FROM " + clazz.getName() + " data";
        if (!whereParams.isEmpty()) {
            jpaQuery += " WHERE ";
            jpaQuery += StringUtils.join(whereParams, " AND ");
        }
        if (orderColumn != null) {
            jpaQuery += " ORDER BY " + orderColumn + " " + orderType;
        }
    }
}
