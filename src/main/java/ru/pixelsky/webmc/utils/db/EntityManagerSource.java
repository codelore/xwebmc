package ru.pixelsky.webmc.utils.db;

import javax.persistence.EntityManager;

public interface EntityManagerSource {
    EntityManager getEntityManager();
}
