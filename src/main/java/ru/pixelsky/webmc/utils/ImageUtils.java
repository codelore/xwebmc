package ru.pixelsky.webmc.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ImageUtils {
    public static BufferedImage imageFromBytes(byte[] bytes) {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        try {
            return ImageIO.read(bis);
        } catch (IOException e) {
            return null;
        }
    }

    public static byte[] imageToBytes(BufferedImage image, String format) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, format, bos);
            return bos.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }

    public static BufferedImage toSize(BufferedImage image, int w, int h) {
        BufferedImage bi = new BufferedImage(w, h, image.getType());
        Graphics2D g = bi.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g.drawImage(image, 0, 0, w, h, null);

        g.dispose();
        return bi;
    }
}
