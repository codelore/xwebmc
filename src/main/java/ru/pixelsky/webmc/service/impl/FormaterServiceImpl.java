package ru.pixelsky.webmc.service.impl;

import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.service.FormatterService;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class FormaterServiceImpl implements FormatterService {
    private ThreadLocal<SimpleDateFormat> tl = new ThreadLocal<SimpleDateFormat>();

    @Override
    public String formatDate(Date d) {
        SimpleDateFormat sdf = tl.get();
        if(sdf == null) {
            sdf = new SimpleDateFormat("hh/MM/yyyy HH:mm:ss");
            tl.set(sdf);
        }
        return sdf.format(d);
    }
}
