package ru.pixelsky.webmc.service.impl;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.exception.BadImageSizeException;
import ru.pixelsky.webmc.exception.CabinetException;
import ru.pixelsky.webmc.service.ContentService;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.*;

@Service
public class ContentServiceImpl implements ContentService {
    private File skinsDir;
    private File cloaksDir;

    private byte[] defaultSkin;

    private Map<String, Future<byte[]>> filesCache = new ConcurrentHashMap<String, Future<byte[]>>();
    private ExecutorService skinLoaderExecutor = Executors.newFixedThreadPool(3);

    public ContentServiceImpl() {
        skinsDir = new File("contents/skins");
        cloaksDir = new File("contents/cloaks");

        skinsDir.mkdirs();
        cloaksDir.mkdirs();
    }

    @Override
    public void setSkin(String playerName, byte[] skin) throws IOException {
        save(skinsDir, playerName, skin);
    }

    @Override
    public byte[] getSkin(String playerName) throws IOException {
        byte[] skin = get(skinsDir, playerName);
        return skin != null ? skin : defaultSkin;
    }

    @Override
    public void setCloak(String playerName, byte[] cloak) throws IOException {
        save(cloaksDir, playerName, cloak);
    }

    @Override
    public byte[] getCloak(String playerName) throws IOException {
        return get(cloaksDir, playerName);
    }

    @PostConstruct
    private void init() throws IOException {
        InputStream is = getClass().getResourceAsStream("/default/char.png");
        try {
            defaultSkin = IOUtils.toByteArray(is);
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    private void save(File dir, String playerName, byte[] bytes) throws IOException {
        File file = new File(dir, getImageFilename(playerName));
        String key = dir.getAbsolutePath() + ":" + playerName;

        if (bytes != null) {
            /* Update skin/cloak */
            bytes = checkAndConvertToPng(bytes);

            OutputStream os = null;
            try {
                os = new FileOutputStream(file);
                os.write(bytes);
            } finally {
                if (os != null)
                    os.close();
            }
        } else {
            /* Delete skin/cloak */
            if (file.exists()) {
                if (!file.delete()) {
                    throw new CabinetException(CabinetException.CODE_UNKNOWN, "Не удалось удалить скин или плащ");
                }
            }
        }

        filesCache.remove(key);
    }

    private byte[] get(File dir, String playerName) throws IOException {
        String key = dir.getAbsolutePath() + ":" + playerName;
        Future<byte[]> f = filesCache.get(key);
        if (f == null) {
            f = skinLoaderExecutor.submit(new LoadDataCallable(dir, playerName));
            filesCache.put(key, f);
        }
        try {
            return f.get();
        } catch (Exception e) {
            throw new IOException("Error loading data for " + playerName, e);
        }
    }

    private class LoadDataCallable implements Callable<byte[]> {
        private File dir;
        private String playerName;

        private LoadDataCallable(File dir, String playerName) {
            this.dir = dir;
            this.playerName = playerName;
        }

        @Override
        public byte[] call() throws Exception {
            File file = new File(dir, getImageFilename(playerName));
            if (!file.exists()) {
                return null;
            }

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            InputStream is = null;
            try {
                is = new FileInputStream(file);
                return IOUtils.toByteArray(is);
            } finally {
                IOUtils.closeQuietly(is);
            }
        }
    }

    private String getImageFilename(String name) {
        try {
            return URLEncoder.encode(name, "UTF-8") + ".png";
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private byte[] checkAndConvertToPng(byte[] bytes) throws IOException {
        Dimension dimension = getImageDimension(bytes);
        if (checkDimension(dimension)) {
            bytes = toPNG(bytes);
            return bytes;
        } else {
            throw new BadImageSizeException();
        }
    }

    private byte[] toPNG(byte[] imageInSomeFormat) throws IOException {
        BufferedImage image = ImageIO.read(new ByteArrayInputStream(imageInSomeFormat));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(image, "PNG", bos);
        return bos.toByteArray();
    }

    private boolean checkDimension(Dimension dimension) {
        if (dimension == null)
            return false;
        int w = (int) dimension.getWidth();
        int h = (int) dimension.getHeight();
        return (w == 22 && h == 17) || (w == 2 * h && (w == 64 || w == 128 || w == 256 || w == 512 || w == 1024));
    }

    private Dimension getImageDimension(byte[] bytes) throws IOException {
        InputStream stream = new ByteArrayInputStream(bytes);
        ImageInputStream in = ImageIO.createImageInputStream(stream);
        final Iterator readers = ImageIO.getImageReaders(in);
        while (readers.hasNext()) {
            ImageReader reader = (ImageReader) readers.next();
            try {
                reader.setInput(in);
                return new Dimension(reader.getWidth(0), reader.getHeight(0));
            } finally {
                reader.dispose();
            }
        }
        return null;
    }
}
