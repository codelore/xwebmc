package ru.pixelsky.webmc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ru.pixelsky.webmc.data.UserData;
import ru.pixelsky.webmc.exception.AccessException;
import ru.pixelsky.webmc.exception.GuestException;
import ru.pixelsky.webmc.service.LoginService;
import ru.pixelsky.webmc.service.UserService;

import javax.servlet.http.HttpSession;

@Service
public class LoginServiceImpl implements LoginService {
    private static final int ACCESS_LEVEL_ADMIN = 10;

    @Autowired
    private UserService userService;

    @Override
    public long getAdminId() {
        long userId = getUserId();
        UserData userData = userService.getUserData(userId);
        if (userData.getAccessLevel() >= ACCESS_LEVEL_ADMIN)
            return userId;
        throw new AccessException("Only for admins");
    }

    @Override
    public long getUserId() {
        Long userid = (Long) session().getAttribute("userid");
        if (userid != null) {
            return userid;
        } else {
            throw new GuestException();
        }
    }

    @Override
    public void setUserId(long id) {
        session().setAttribute("userid", id);
    }

    @Override
    public void logout() {
        session().removeAttribute("userid");
    }

    public static HttpSession session() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true);
    }
}
