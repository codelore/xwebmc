package ru.pixelsky.webmc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.exception.LoginException;
import ru.pixelsky.webmc.service.AuthService;
import ru.pixelsky.webmc.service.UserService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

@Service
public class TempKeyAuthService implements AuthService {
    private static final String url = "http://pixelsky.ru/xf-checkkey.php?username=%username%&key=%key%";

    @Autowired
    private UserService userService;

    @Override
    public long authorizeUser(String username, String key) {
        String eusername = urlEncode(username);
        String ekey = urlEncode(key);

        String u = url.replaceAll("%username%", eusername).replaceAll("%key%", ekey);
        try {
            if (getResponse(u).equals("OK"))
                return userService.getUserData(username).getId();
            else
                throw new LoginException();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public String getResponse(String urls) throws IOException {
        URL url;
        HttpURLConnection conn;
        BufferedReader rd = null;
        String line;
        String result = "";
        try {
            url = new URL(urls);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = rd.readLine()) != null) {
                result += line;
                if (rd.ready())
                    result += '\n';
            }
            rd.close();
        } finally {
            if (rd != null)
                rd.close();
        }
        return result;
    }

}
