package ru.pixelsky.webmc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.data.MoneyData;
import ru.pixelsky.webmc.exception.CabinetException;
import ru.pixelsky.webmc.exception.NotEnoughMoneyException;
import ru.pixelsky.webmc.service.DatabaseService;
import ru.pixelsky.webmc.service.MoneyService;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class MoneyServiceImpl implements MoneyService {
    @Autowired
    private DatabaseService databaseService;

    @Override
    public void put(long id, long value) {
        EntityManager em = databaseService.getEntityManager();
        try {
            em.getTransaction().begin();
            MoneyData data = query(em, id);
            data.setMoney(data.getMoney() + value);
            em.merge(data);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Override
    public void take(long id, long value) {
        EntityManager em = databaseService.getEntityManager();
        try {
            em.getTransaction().begin();
            MoneyData data = query(em, id);
            if (data.getMoney() < value)
                throw new NotEnoughMoneyException();
            data.setMoney(data.getMoney() - value);
            em.merge(data);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Override
    public long query(long id) {
        EntityManager em = databaseService.getEntityManager();
        try {
            return query(em, id).getMoney();
        } finally {
            em.close();
        }
    }

    private MoneyData query(EntityManager em, long id) {
        List<MoneyData> resultList = em.createQuery("SELECT md FROM ru.pixelsky.webmc.data.MoneyData md WHERE id=:id", MoneyData.class)
                .setParameter("id", id).getResultList();
        if (!resultList.isEmpty())
            return resultList.get(0);
        return new MoneyData(id);
    }
}
