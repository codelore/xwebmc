package ru.pixelsky.webmc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.service.ContentService;
import ru.pixelsky.webmc.service.SkinFormsService;
import ru.pixelsky.webmc.utils.ImageUtils;
import ru.pixelsky.webmc.utils.SkinPart;
import ru.pixelsky.webmc.utils.SkinUtils;

import java.awt.image.BufferedImage;
import java.io.IOException;

@Service
public class SkinFormsServiceImpl implements SkinFormsService {
    @Autowired
    private ContentService contentService;

    @Override
    public byte[] getFace(String playerName) throws IOException {
        BufferedImage headImage = getHeadImage(playerName);
        if (headImage != null) {
            BufferedImage sizedHead = ImageUtils.toSize(headImage, 48, 48);
            return ImageUtils.imageToBytes(sizedHead, "PNG");
        } else {
            return null;
        }
    }

    @Override
    public byte[] getFrontSkin(String playerName) throws IOException {
        BufferedImage skinImage = getSkinImage(playerName);
        if (skinImage != null) {
            BufferedImage frontSkin = ImageUtils.toSize(SkinUtils.getFrontSkin(skinImage), 96, 192);
            return ImageUtils.imageToBytes(frontSkin, "PNG");
        } else {
            return null;
        }
    }

    private BufferedImage getHeadImage(String playerName) throws IOException {
        BufferedImage skinImage = getSkinImage(playerName);
        if (skinImage != null) {
            return SkinPart.HEAD_FRONT.getPartImage(skinImage);
        } else {
            return null;
        }
    }

    private BufferedImage getSkinImage(String playerName) throws IOException {
        byte[] skinBytes = contentService.getSkin(playerName);
        if (skinBytes != null) {
            return ImageUtils.imageFromBytes(skinBytes);
        } else {
            return null;
        }
    }
}
