package ru.pixelsky.webmc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.dao.ShopDAO;
import ru.pixelsky.webmc.data.ShopCartItem;
import ru.pixelsky.webmc.data.ShopItem;
import ru.pixelsky.webmc.data.UserData;
import ru.pixelsky.webmc.exception.NoSuchShopItemException;
import ru.pixelsky.webmc.exception.NoSuchUserException;
import ru.pixelsky.webmc.exception.NotEnoughMoneyException;
import ru.pixelsky.webmc.service.MoneyService;
import ru.pixelsky.webmc.service.ShopService;
import ru.pixelsky.webmc.service.UserService;

import java.math.BigInteger;
import java.util.List;
import java.util.logging.Logger;

@Service
public class ShopServiceImpl implements ShopService {
    private static final Logger log = Logger.getLogger(ShopServiceImpl.class.getName());

    @Autowired
    private UserService userService;

    @Autowired
    private ShopDAO dao;

    @Autowired
    private MoneyService moneyService;

    @Override
    public void giveItems(long userId, ShopCartItem item) {
        UserData userData = userService.getUserData(userId);
        if (userData != null) {
            String userName = userData.getName();
            item.setPlayer(userName);
            dao.persistCartItem(item);

            log.info("Given #" + item.getId() + " (" + item.getItem() + ") x " + item.getAmount() + " to user#" + userId);
        } else {
            throw new NoSuchUserException(userId);
        }
    }

    @Override
    public void buyItem(long userId, int itemId, int quantity) {
        ShopItem item = dao.findShopItem(itemId);
        if (item != null) {
            /* Защита от ошибок переполнения: quantity*price настолько большой, что long переполняется */
            BigInteger totalAmount = BigInteger.valueOf(item.getPrice()).multiply(BigInteger.valueOf(quantity));
            if (totalAmount.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0)
                throw new NotEnoughMoneyException();
            long tamount = totalAmount.longValue();

            moneyService.take(userId, tamount);
            log.info("Taken " + tamount + " from user#" + userId + " for buying " + quantity + "x " + item.asJSON());

            giveItems(userId, ShopCartItem.valueOf(item, null, quantity));
        } else {
            throw new NoSuchShopItemException(itemId);
        }
    }

    @Override
    public List<ShopItem> listItems() {
        return dao.listShopItems();
    }
}
