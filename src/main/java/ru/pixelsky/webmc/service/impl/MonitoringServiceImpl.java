package ru.pixelsky.webmc.service.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.service.MonitoringService;
import ru.pixelsky.webmc.data.MonitoredServer;
import ru.pixelsky.webmc.data.ServerState;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Service
public class MonitoringServiceImpl implements MonitoringService {
    private final static int TIMEOUT = 1000;
    private final static long PERIOD = 60000;

    private ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(1);
    private List<MonitoredServer> monitoredServers = Collections.emptyList();
    private volatile List<ServerState> serverStates;

    private boolean working;

    public MonitoringServiceImpl() {
        monitoredServers = Arrays.asList(
                new MonitoredServer("10.0.0.1", 25566, "Sandbox")
        );
    }

    @Override
    public List<ServerState> getMonitoringResult() {
        return serverStates;
    }

    @PostConstruct
    private void startMonitoring() {
        synchronized (this) {
            if (!working) {
                executorService.scheduleAtFixedRate(new Runnable() {
                    @Override
                    public void run() {
                        updateServerStates();
                    }
                }, 0, PERIOD, TimeUnit.MILLISECONDS);
                working = true;
            }
        }
    }

    @PreDestroy
    private void stopMonitoring() {
        executorService.shutdownNow();
        synchronized (this) {
            working = false;
        }
    }

    private void updateServerStates() {
        List<MonitoredServer> servers = monitoredServers;
        List<ServerState> list = new ArrayList<ServerState>(servers.size());

        for (MonitoredServer server : servers) {
            list.add(getServerState(server));
        }

        serverStates = list;
    }

    private ServerState getServerState(MonitoredServer serverData)
    {      
        ServerState state = new ServerState();
        BufferedReader in = null;
        PrintWriter out = null;
        Socket socket = null;
        String message = null;
        JsonElement json = null;
        state.setName(serverData.getName());

        try
        {
            socket = new Socket();
            socket.setSoTimeout(TIMEOUT);
            socket.setTcpNoDelay(true);
            socket.setTrafficClass(18);
            socket.connect(new InetSocketAddress(serverData.getHost(), serverData.getPort()), TIMEOUT);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
            out.println("QUERY_JSON");
            out.flush();
            while((message = in.readLine()) != null)
                json = readFromJSON(message);
            
            JsonObject jobject = json.getAsJsonObject();
            if(!jobject.isJsonNull())
            {
                state.setPlayersOnline(jobject.get("playerCount").getAsInt());
                state.setPlayersMax(jobject.get("maxPlayers").getAsInt());
                state.setOnline(true);
            }

        } catch (IOException ignored) {
            state.setOnline(false);
        } finally
        {
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(out);
            closeSocketQuietly(socket);
        }
        return state;
    }

  
    private static JsonElement readFromJSON(String input)
    {
        JsonElement jsonObject = new JsonParser().parse(input);
        return jsonObject;
    }
    
    /**
     * Copied from minecraft
     */
    
    public static String readString(DataInputStream input, int maxLength) throws IOException
    {
        short short1 = input.readShort();

        if (short1 > maxLength)
        {
            throw new IOException("Received string length longer than maximum allowed (" + short1 + " > " + maxLength + ")");
        }
        else if (short1 < 0)
        {
            throw new IOException("Received string length is less than zero! Weird string!");
        }
        else
        {
            StringBuilder stringbuilder = new StringBuilder();

            for (int j = 0; j < short1; ++j)
            {
                stringbuilder.append(input.readChar());
            }

            return stringbuilder.toString();
        }
    }

    private void closeSocketQuietly(Socket socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException ignored) {

            }
        }
    }
}