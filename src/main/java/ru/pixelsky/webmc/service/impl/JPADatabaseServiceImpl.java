package ru.pixelsky.webmc.service.impl;

import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.service.DatabaseService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Service
public class JPADatabaseServiceImpl implements DatabaseService {
    private EntityManagerFactory entityManagerFactory;

    public JPADatabaseServiceImpl() {
        entityManagerFactory = Persistence.createEntityManagerFactory("ru.pixelsky.webmc");
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
}
