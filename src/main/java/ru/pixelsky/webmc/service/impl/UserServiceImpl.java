package ru.pixelsky.webmc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.dao.UserDAO;
import ru.pixelsky.webmc.data.UserData;
import ru.pixelsky.webmc.service.UserService;

import java.util.Date;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO dao;

    @Override
    public UserData getUserData(long id) {
        return dao.find(id);
    }

    @Override
    public UserData getUserData(String name) {
        UserData userData = dao.find(name);
        if (userData != null) {
            return userData;
        } else {
            userData = new UserData();
            userData.setName(name);
            userData.setRegistrationDate(new Date());
            dao.persist(userData);
            return userData;
        }
    }
}
