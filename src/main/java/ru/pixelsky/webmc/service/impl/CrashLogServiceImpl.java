package ru.pixelsky.webmc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pixelsky.webmc.dao.CrashLogDAO;
import ru.pixelsky.webmc.dao.UserDAO;
import ru.pixelsky.webmc.data.CrashLog;
import ru.pixelsky.webmc.data.UserData;
import ru.pixelsky.webmc.service.CrashLogService;
import ru.pixelsky.webmc.service.UserService;

import java.util.Date;
import java.util.List;

@Service
public class CrashLogServiceImpl implements CrashLogService {
    @Autowired
    private CrashLogDAO dao;

    @Override
    public void log(String username, String text) {
        CrashLog cl = new CrashLog();
        cl.setUser(username);
        cl.setLog(text);
        dao.persist(cl);
    }

    @Override
    public List<CrashLog> findLogs(String username) {
        return dao.find(username);
    }

    @Override
    public List<CrashLog> allLogs() {
        return dao.find();
    }
}
