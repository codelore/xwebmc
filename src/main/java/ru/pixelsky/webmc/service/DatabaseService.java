package ru.pixelsky.webmc.service;

import ru.pixelsky.webmc.utils.db.EntityManagerSource;

public interface DatabaseService extends EntityManagerSource {
}
