package ru.pixelsky.webmc.service;

import ru.pixelsky.webmc.data.UserData;

public interface UserService {
    UserData getUserData(long id);
    UserData getUserData(String name);
}
