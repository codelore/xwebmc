package ru.pixelsky.webmc.service;

import java.util.Date;

public interface FormatterService {
    String formatDate(Date date);
}
