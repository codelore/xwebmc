package ru.pixelsky.webmc.service;

import ru.pixelsky.webmc.data.ShopCartItem;
import ru.pixelsky.webmc.data.ShopItem;

import java.util.List;

public interface ShopService {
    void giveItems(long userId, ShopCartItem item);
    void buyItem(long userId, int itemId, int amount);
    List<ShopItem> listItems();
}
