package ru.pixelsky.webmc.service;

import java.io.IOException;

public interface SkinFormsService {
    byte[] getFace(String playerName) throws IOException;
    byte[] getFrontSkin(String playerName) throws IOException;
}
