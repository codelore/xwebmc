package ru.pixelsky.webmc.service;

public interface LoginService {
    long getAdminId();
    long getUserId();
    void setUserId(long id);
    void logout();
}