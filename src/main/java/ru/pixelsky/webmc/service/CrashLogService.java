package ru.pixelsky.webmc.service;

import ru.pixelsky.webmc.data.CrashLog;

import java.util.List;

public interface CrashLogService {
    void log(String username, String text);
    List<CrashLog> findLogs(String username);
    List<CrashLog> allLogs();
}
