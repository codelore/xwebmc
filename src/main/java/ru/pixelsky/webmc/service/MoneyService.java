package ru.pixelsky.webmc.service;

public interface MoneyService {
    void put(long id, long value);
    void take(long id, long value);
    long query(long id);
}
