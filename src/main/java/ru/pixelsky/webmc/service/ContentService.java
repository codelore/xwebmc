package ru.pixelsky.webmc.service;

import java.io.IOException;

public interface ContentService {
    void setSkin(String playerName, byte[] skin) throws IOException;
    byte[] getSkin(String playerName) throws IOException;

    void setCloak(String playerName, byte[] skin) throws IOException;
    byte[] getCloak(String playerName) throws IOException;
}
