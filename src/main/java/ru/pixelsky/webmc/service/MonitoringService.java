package ru.pixelsky.webmc.service;

import ru.pixelsky.webmc.data.ServerState;

import java.util.List;

public interface MonitoringService {
    List<ServerState> getMonitoringResult();
}
