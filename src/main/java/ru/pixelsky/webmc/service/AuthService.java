package ru.pixelsky.webmc.service;

public interface AuthService {
    long authorizeUser(String username, String password);
}
