package ru.pixelsky.webmc.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "webmc_shopcart")
public class ShopCartItem {
    private Long id;
    private String type = "item";
    private String player;
    private String item;
    private int amount = 1;
    private String extra;

    @Id
    @Column(name = "id")
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "type")
    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "player")
    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    @Column(name = "item")
    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    @Column(name = "amount")
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Column(name = "extra")
    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public static ShopCartItem valueOf(ShopItem shopItem, String player, int amount) {
        ShopCartItem cartItem = new ShopCartItem();
        cartItem.amount = amount;
        cartItem.player = player;
        cartItem.extra = shopItem.getExtra();
        cartItem.item = shopItem.getItem();
        cartItem.type = shopItem.getType();
        return cartItem;
    }
}
