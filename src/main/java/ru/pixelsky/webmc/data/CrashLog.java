package ru.pixelsky.webmc.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity(name = "webmc_crashlogs")
public class CrashLog {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column
    private String user;

    @Column
    private Date date = new Date();

    @Column(length = 64000, columnDefinition = "TEXT")
    private String log;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}
