package ru.pixelsky.webmc.data;

import org.hibernate.annotations.Index;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity(name = "webmc_users")
public class UserData {
    @Id
    @GeneratedValue
    @Column(name = "user_id")
    private Long id;

    @Index(name = "index_name")
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "reg_date", nullable = true)
    private Date registrationDate;

    @Column(name = "access_level", nullable = false)
    private int accessLevel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }
}
