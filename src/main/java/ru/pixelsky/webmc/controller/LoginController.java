package ru.pixelsky.webmc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.pixelsky.webmc.dao.GameBalanceDAO;
import ru.pixelsky.webmc.data.UserData;
import ru.pixelsky.webmc.service.AuthService;
import ru.pixelsky.webmc.service.LoginService;
import ru.pixelsky.webmc.service.MoneyService;
import ru.pixelsky.webmc.service.UserService;

import java.math.BigDecimal;

@Controller
public class LoginController {
    @Autowired
    private LoginService loginService;

    @Autowired
    private MoneyService moneyService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthService authService;

    @Autowired
    private GameBalanceDAO gameBalanceService;

    @ResponseBody
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public Object logout() {
        loginService.logout();
        return Responses.successResponse();
    }

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public LoginResponse login(@RequestParam String login, @RequestParam String password) {
        long userId = authService.authorizeUser(login, password);
        loginService.setUserId(userId);

        return new LoginResponse(userId);
    }


    @RequestMapping(value = "/player-info", method = RequestMethod.GET)
    @ResponseBody
    public InfoResponse info() {
        UserData data = userService.getUserData(loginService.getUserId());
        return new InfoResponse(data.getId(), data.getName(), moneyService.query(data.getId()),
                gameBalanceService.findBalance(data.getName()).getBalance());
    }

    public static class LoginResponse {
        public long id;

        public LoginResponse(long id) {
            this.id = id;
        }
    }

    public static class InfoResponse {
        public long id;
        public String name;
        public long realmoney;
        public BigDecimal sandboxBalance;

        public InfoResponse(long id, String name, long realmoney, BigDecimal sandboxBalance) {
            this.id = id;
            this.name = name;
            this.realmoney = realmoney;
            this.sandboxBalance = sandboxBalance;
        }
    }
}
