package ru.pixelsky.webmc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.pixelsky.webmc.data.ShopItem;
import ru.pixelsky.webmc.service.LoginService;
import ru.pixelsky.webmc.service.ShopService;

import java.util.List;

@Controller
public class ShopController {
    @Autowired
    private ShopService shopService;

    @Autowired
    private LoginService loginService;

    @ResponseBody
    @RequestMapping(value = "/shop-list", method = RequestMethod.GET)
    public ShopListResponse listShopItems(ModelMap model) {
        List<ShopItem> shopItems = shopService.listItems();
        return new ShopListResponse(shopItems);
    }

    @ResponseBody
    @RequestMapping(value = "/shop-buy", method = RequestMethod.POST)
    public Object buyShopItem(@RequestParam int itemId, @RequestParam int amount) {
        shopService.buyItem(loginService.getUserId(), itemId, amount);
        return Responses.successResponse();
    }

    public static class ShopListResponse {
        public List<ShopItem> list;

        public ShopListResponse(List<ShopItem> list) {
            this.list = list;
        }
    }
}
