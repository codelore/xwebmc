package ru.pixelsky.webmc.controller;

import java.util.List;

public class Responses {
    private static final SuccessResponse successResponse = new SuccessResponse(true);
    private static final SuccessResponse failResponse = new SuccessResponse(false);

    public static SuccessResponse successResponse() {
        return successResponse;
    }

    public static SuccessResponse failResponse() {
        return failResponse;
    }

    public static <T> ListWrapper<T> wrapList(List<T> list) {
        return new ListWrapper<T>(list);
    }

    public static class SuccessResponse {
        public boolean success;

        private SuccessResponse(boolean success) {
            this.success = success;
        }
    }

    public static class ListWrapper<T> {
        public List<T> list;

        public ListWrapper(List<T> list) {
            this.list = list;
        }
    }
}
