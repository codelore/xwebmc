package ru.pixelsky.webmc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.pixelsky.webmc.data.UserData;
import ru.pixelsky.webmc.service.ContentService;
import ru.pixelsky.webmc.service.LoginService;
import ru.pixelsky.webmc.service.SkinFormsService;
import ru.pixelsky.webmc.service.UserService;

import java.io.IOException;

@Controller
public class ContentController {
    @Autowired
    private ContentService contentService;

    @Autowired
    private SkinFormsService skinFormsService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/skin/{name}.png", "/content/skins/{name}.png"}, method = RequestMethod.GET)
    private ResponseEntity<byte[]> getSkin(@PathVariable String name) throws IOException {
        byte[] data = contentService.getSkin(name);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.IMAGE_PNG);

        if (data != null)
            return new ResponseEntity<byte[]>(data, responseHeaders, HttpStatus.OK);
        else
            return new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = {"/cloak/{name}.png", "/content/cloaks/{name}.png"}, method = RequestMethod.GET)
    private ResponseEntity<byte[]> getCloak(@PathVariable String name) throws IOException {
        byte[] data = contentService.getCloak(name);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.IMAGE_PNG);

        if (data != null)
            return new ResponseEntity<byte[]>(data, responseHeaders, HttpStatus.OK);
        else
            return new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = {"/faces/{name}.png", "/content/faces/{name}.png"}, method = RequestMethod.GET)
    private ResponseEntity<byte[]> getFace(@PathVariable String name) throws IOException {
        byte[] data = skinFormsService.getFace(name);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.IMAGE_PNG);

        if (data != null)
            return new ResponseEntity<byte[]>(data, responseHeaders, HttpStatus.OK);
        else
            return new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = {"/frontskins/{name}.png", "/content/frontskins/{name}.png"}, method = RequestMethod.GET)
    private ResponseEntity<byte[]> getFrontskin(@PathVariable String name) throws IOException {
        byte[] data = skinFormsService.getFrontSkin(name);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.IMAGE_PNG);

        if (data != null)
            return new ResponseEntity<byte[]>(data, responseHeaders, HttpStatus.OK);
        else
            return new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    private Object uploadContent(@RequestParam(required = false) MultipartFile skin,
                                 @RequestParam(required = false) MultipartFile cloak) throws IOException {
        UserData data = userService.getUserData(loginService.getUserId());
        String name = data.getName();

        if (skin != null && !skin.isEmpty()) {
            contentService.setSkin(name, skin.getBytes());
        }
        if (cloak != null && !cloak.isEmpty()) {
            contentService.setCloak(name, cloak.getBytes());
        }
        return Responses.successResponse();
    }

    @RequestMapping(value = "/contentdelete/skin", method = RequestMethod.POST)
    @ResponseBody
    private Object deleteSkin() throws IOException {
        UserData data = userService.getUserData(loginService.getUserId());
        String name = data.getName();
        contentService.setSkin(name, null);
        return Responses.successResponse();
    }

    @RequestMapping(value = "/contentdelete/cloak", method = RequestMethod.POST)
    @ResponseBody
    private Object deleteCloakk() throws IOException {
        UserData data = userService.getUserData(loginService.getUserId());
        String name = data.getName();
        contentService.setCloak(name, null);
        return Responses.successResponse();
    }

    @RequestMapping(value = "/upload", method = RequestMethod.GET)
    private String uploadPage() {
        return "upload";
    }
}
