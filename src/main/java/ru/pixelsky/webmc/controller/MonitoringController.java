package ru.pixelsky.webmc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.pixelsky.webmc.data.ServerState;
import ru.pixelsky.webmc.service.MonitoringService;

import java.util.List;

@Controller
public class MonitoringController {
    @Autowired
    private MonitoringService monitoringService;

    @RequestMapping("/monitoring.json")
    @ResponseBody
    private List<ServerState> getMonitoringData() {
        return monitoringService.getMonitoringResult();
    }
}
