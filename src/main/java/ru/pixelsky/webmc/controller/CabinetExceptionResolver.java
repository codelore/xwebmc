package ru.pixelsky.webmc.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.pixelsky.webmc.exception.CabinetException;

@ControllerAdvice
public class CabinetExceptionResolver {
    @ResponseBody
    @ExceptionHandler(CabinetException.class)
    protected CabinetExceptionResponse handleException(CabinetException ex) {
        return new CabinetExceptionResponse(ex);
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    protected CabinetExceptionResponse handleException(Exception ex) {
        ex.printStackTrace();
        return new CabinetExceptionResponse(new CabinetException(CabinetException.CODE_UNKNOWN, "Internal Server Error"));
    }

    public static class CabinetExceptionResponse {
        public PublicCabinetException error;

        public CabinetExceptionResponse(CabinetException error) {
            this.error = new PublicCabinetException(error);
        }
    }

    public static class PublicCabinetException {
        public int code;
        public String message;

        public PublicCabinetException(CabinetException ex) {
            this.code = ex.getCode();
            this.message = ex.getMessage();
        }
    }
}
