package ru.pixelsky.webmc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.pixelsky.webmc.service.LoginService;
import ru.pixelsky.webmc.service.MoneyService;

@Controller
public class MoneyController {
    @Autowired
    private LoginService loginService;

    @Autowired
    private MoneyService moneyService;

    @ResponseBody
    @RequestMapping(value = "/money-put", method = RequestMethod.GET)
    public Object putMoney(@RequestParam int amount) {
        long userId = loginService.getAdminId();
        moneyService.put(userId, amount);
        return Responses.successResponse();
    }

    @ResponseBody
    @RequestMapping(value = "/money-take", method = RequestMethod.GET)
    public Object takeMoney() {
        long userId = loginService.getUserId();
        moneyService.take(userId, 15);
        return Responses.successResponse();
    }
}
