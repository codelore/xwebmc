package ru.pixelsky.webmc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.pixelsky.webmc.data.CrashLog;
import ru.pixelsky.webmc.service.CrashLogService;
import ru.pixelsky.webmc.service.LoginService;

@Controller
public class CrashLogController {
    @Autowired
    private LoginService loginService;

    @Autowired
    private CrashLogService crashLogService;

    @RequestMapping(value = "/crash-logs-add", method = RequestMethod.POST)
    @ResponseBody
    private Object addLog(@RequestParam String username, @RequestParam String log) {
        crashLogService.log(username, log);
        return Responses.successResponse();
    }

    @RequestMapping(value = "/crash-logs-user", method = RequestMethod.GET)
    @ResponseBody
    private Responses.ListWrapper<CrashLog> findLogByUser(@RequestParam String username) {
        return Responses.wrapList(crashLogService.findLogs(username));
    }

    @RequestMapping(value = "/crash-logs-all", method = RequestMethod.GET)
    @ResponseBody
    private Responses.ListWrapper<CrashLog> findAll() {
        return Responses.wrapList(crashLogService.allLogs());
    }
}
