package ru.pixelsky.webmc.exception;

public class LoginException extends CabinetException {
    public LoginException() {
        super(LOGIN_PASSWORD_MISMATCH, "Неверная пара логин/пароль");
    }
}
