package ru.pixelsky.webmc.exception;

public class NoSuchUserException extends CabinetException {
    public NoSuchUserException(String userName) {
        super(INVALID_PARAM, userName);
    }

    public NoSuchUserException(long userId) {
        super(INVALID_PARAM, "#" + userId);
    }
}
