package ru.pixelsky.webmc.exception;

public class GuestException extends CabinetException {
    public GuestException() {
        super(NOT_AUTHORIZED, "Вы не авторизованы");
    }
}
