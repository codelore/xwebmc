package ru.pixelsky.webmc.exception;

public class NoSuchShopItemException extends CabinetException {
    private int itemId;

    public NoSuchShopItemException(int itemId) {
        super(INVALID_PARAM, "Такого товара нет в магазине: #" + itemId);
        this.itemId = itemId;
    }

    public int getItemId() {
        return itemId;
    }
}
