package ru.pixelsky.webmc.exception;

public class AccessException extends CabinetException {
    public AccessException(String message) {
        super(ACCESS_DENIED, message);
    }
}
