package ru.pixelsky.webmc.exception;

public class BadImageSizeException extends CabinetException {
    public BadImageSizeException() {
        super(BAD_IMAGE_SIZE, "Неверный размер изображения");
    }
}
