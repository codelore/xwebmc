package ru.pixelsky.webmc.exception;

public class CabinetException extends RuntimeException {
    public static final int CODE_UNKNOWN = 1;
    public static final int INVALID_PARAM = 2;
    public static final int BAD_CHEKSUM = 3;
    public static final int NOT_AUTHORIZED = 4;
    public static final int TOO_LOW_BALANCE = 5;
    public static final int BAD_IMAGE_SIZE = 6;
    public static final int LOGIN_PASSWORD_MISMATCH = 7;
    public static final int ALREADY_VOTED = 8;
    public static final int ACCESS_DENIED = 42;

    private int code;

    public CabinetException(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
