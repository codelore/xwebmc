package ru.pixelsky.webmc.exception;

public class NotEnoughMoneyException extends CabinetException {
    public NotEnoughMoneyException() {
        super(TOO_LOW_BALANCE, "Не хватает денег на счету");
    }
}
